#I/O library-----------------------------------------------------------------#

import Adafruit_ADS1x15 as ADS
import board
import digitalio

#UI library------------------------------------------------------------------#

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.widgets import Button

#Processing library----------------------------------------------------------#

import multiprocessing as mp
from itertools import count
from threading import Thread
from time import strftime, sleep, time

#I/O variable----------------------------------------------------------------#

power = digitalio.DigitalInOut(board.D24)
power.direction = digitalio.Direction.OUTPUT
pump = digitalio.DigitalInOut(board.D18)
pump.direction = digitalio.Direction.OUTPUT
air_valve = digitalio.DigitalInOut(board.D23)
air_valve.direction = digitalio.Direction.OUTPUT
obj_valve = digitalio.DigitalInOut(board.D25)
obj_valve.direction = digitalio.Direction.OUTPUT

power.value = True
pump.value = True
air_valve.value = True
obj_valve.value = True

#I2C data reading loop-------------------------------------------------------#

adc1 = ADS.ADS1115(address=0x48)
adc2 = ADS.ADS1115(address=0x4a)

Gain = 1

time_vals = []
sensor1_vals = []
sensor2_vals = []
sensor3_vals = []
sensor4_vals = []
sensor5_vals = []
sensor6_vals = []

index = count(0,2)

def sensorData():
    while True:
        sensorData.sensor1 = ((adc1.read_adc(0, Gain)/32768)*5500)
        sensorData.sensor2 = ((adc1.read_adc(1, Gain)/32768)*5500)
        sensorData.sensor3 = ((adc1.read_adc(2, Gain)/32768)*5500)
        sensorData.sensor4 = ((adc2.read_adc(0, Gain)/32768)*5500)
        sensorData.sensor5 = ((adc2.read_adc(1, Gain)/32768)*5500)
        sensorData.sensor6 = ((adc2.read_adc(2, Gain)/32768)*5500)
        
sensorThread = Thread(target=sensorData)
sensorThread.start()

#Mechanics loop--------------------------------------------------------------#

def airflow():
    def delay():
        counter = time() + 5
        while time() < counter:
            pump.value = False
            air_valve.value = True
            obj_valve.value = False
            
    def sampling():
        counter = time() + 25
        while time() < counter:
            pump.value = False
            air_valve.value = False
            obj_valve.value = True
            
    def purging():
        counter = time() + 70
        while time() < counter:
            pump.value = False
            air_valve.value = True
            obj_valve.value = False
            
    delay()
    sampling()
    purging()
    pump.value = True
    air_valve.value = True
    obj_valve.value = True

#Data logging to .csv--------------------------------------------------------#

def datalog():
    def mycsv():
        with open("./Datalog/Data_log_"+
                  strftime("%d-%m-%Y_%H:%M")+".csv", "a") as log:
            
            log.write("Waktu,TGS-822,TGS-2600,TGS-2611,MQ-3,"
                      "MQ-9,MQ-135\n")
            x = 0
            while x < 100:
                x += 0.1
                log.write("{0},{1},{2},{3},{4},{5},{6}\n"
                          .format(round(x,1),
                           (round(sensorData.sensor1,1)),
                           (round(sensorData.sensor2,1)),
                           (round(sensorData.sensor3,1)),
                           (round(sensorData.sensor4,1)),
                           (round(sensorData.sensor5,1)),
                           (round(sensorData.sensor6,1))))
                
                sleep(0.1)
            print("File Ready")
    mycsv()

#Multiprocessing program loop------------------------------------------------#

def main_process(self):
    def threading():
        thread1 = Thread(target=datalog)
        thread1.start()
        
    def multicore():
        multi1 = mp.Process(target=airflow)
        multi1.start()
        
    threading()
    multicore()

#UI live graph---------------------------------------------------------------#

Fig, ax = plt.subplots()
Fig.subplots_adjust(bottom=0.25)

def animate(i):
    time_vals.append(next(index))
    sensor1_vals.append(sensorData.sensor1)
    sensor2_vals.append(sensorData.sensor2)
    sensor3_vals.append(sensorData.sensor3)
    sensor4_vals.append(sensorData.sensor4)
    sensor5_vals.append(sensorData.sensor5)
    sensor6_vals.append(sensorData.sensor6)
    
    ax.cla()
    ax.plot(time_vals, sensor1_vals, label="TGS-2611")
    ax.plot(time_vals, sensor2_vals, label="TGS-2600")
    ax.plot(time_vals, sensor3_vals, label="TGS-822")
    ax.plot(time_vals, sensor4_vals, label="MQ-3")
    ax.plot(time_vals, sensor5_vals, label="MQ-9")
    ax.plot(time_vals, sensor6_vals, label="MQ-135")
    ax.legend(loc='upper right', ncol=1)

    ax.set_ylabel("Tegangan (mV)")

ani = animation.FuncAnimation(Fig, animate, interval=50)

#Button function on UI-------------------------------------------------------#

axbutton1 = plt.axes([0.75, 0.05, 0.15, 0.075])
button1 = Button(axbutton1, 'Ambil Data')
button1.on_clicked(main_process)

plt.show()