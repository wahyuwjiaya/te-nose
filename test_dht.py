from time import sleep
import Adafruit_DHT

dhtDevice = Adafruit_DHT.DHT11
dhtPin = 17

tem_val = []
hum_val = []

while True:
    humidity, temperature = Adafruit_DHT.read_retry(dhtDevice, dhtPin)
    temp = float("{:.1f}".format(temperature))
    hum = float("{}".format(humidity))
    
    tem_val.append(temp)
    hum_val.append(hum)
    print(str(tem_val[-1])+", "+str(hum_val[-1]))