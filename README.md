# Te-Nose

Electronic Nose using 6 gas sensors array and mechanical components controlled by Raspberry Pi 3 B+.

Gas sensors used in this project:
1. Figaro TGS-822
2. Figaro TGS-2600
3. Figaro TGS-2611
4. Hanwei MQ-3
5. Hanwei MQ-9
6. Hanwei MQ 135

Components used in this project:
1. 12V Pump
2. 2x 12V Valve
3. 2x 4 Channel I2C ADS1115
4. 5V 4 Channel Relay
5. Logic Level Converter
6. 12V 5A Power Supply
7. 2x 12V 8x8mm Cooling Fan
8. Step Down Buck Converter
